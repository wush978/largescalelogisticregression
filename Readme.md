# Install

## R

```sh
sudo apt-get install r-base r-base-dev
```

## Redis

```sh
sudo apt-get install redis-server
```

## libcurl-dev

```sh
sudo apt-get install libcurl4-openssl-dev
```

## libboost-thread-dev

```sh
sudo apt-get install libboost-thread-dev
```

## Packages

1. Open R Console(Execute `R` directly from shell)
1. Run followign R commands:

    ```r
    options(repos=structure(c(CRAN="http://cran.cs.pu.edu.tw")))
    install.packages("Rcpp")
    install.packages("devtools")
    install.packages("foreach")
    install.packages("rredis")
    install.packages("doRedis")
    library(devtools)
    install_bitbucket("LargeScaleLogisticRegression", "wush978")
    ```
1. Close `R`. Install `git`. Clone the repository of `https://bitbucket.org/wush978/largescalelogisticregression.git`
1. Switch to the `largescalelogisticregressiontests` direcotory
1. Execute `tests/lr_doRedis.test.R`.
    1. Run from shell

        ```sh
        Rscript tests/lr_doRedis.test.R
        ```
    1. Run from R Console. Open `R` console and run:

        ```r
        source("tests/lr_doRedis.test.R")
        ```
1. Cleaning
    1. Clean the radis-server:

        ```sh
        redis-cli "flushdb"
        ```
    1. Kill R process of slaves:

        ```sh
        kill `pidof R`
        ```
