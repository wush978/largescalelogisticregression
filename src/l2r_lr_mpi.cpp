/*
 * l2r_lr_mpi.cpp
 *
 *  Created on: May 30, 2013
 *      Author: wush
 */

#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include "l2r_lr_mpi.hpp"

using namespace Rcpp;

//#define BAYLRL_DEBUG

l2r_lr_mpi::l2r_lr_mpi(SEXP Rmpi_fun, SEXP Rmpi_grad, SEXP Rmpi_Hs, SEXP Rlevels, SEXP Ris_regularize_bias)
: mpi_fun(Rmpi_fun), mpi_grad(Rmpi_grad), mpi_Hs(Rmpi_Hs), is_regularize_bias(as<bool>(Ris_regularize_bias)) {
	List levels(Rlevels);
	n = 1; // intercept
	for(int i = 0;i < levels.size();i++) {
		SEXP pele = wrap(levels[i]);
		if (pele == R_NilValue) { //numerical data
			n++;
			continue;
		}
		n += Rf_length(pele) - 1; // categorical data
	}
#ifdef BAYLRL_DEBUG
	Rprintf("n: %d\n", n);
#endif
}

l2r_lr_mpi::~l2r_lr_mpi() { }

double l2r_lr_mpi::fun(double *w) {
	NumericVector Rw(n);
	::memcpy(&Rw[0], w, sizeof(double) * n);
	double retval_regularization;
	boost::thread_group tgroup;
	tgroup.create_thread(boost::bind(&l2r_lr_mpi::regularize, this, w, &retval_regularization));
	double retval_loglikelihood = as<double>(mpi_fun(wrap(Rw)));
	tgroup.join_all();
	return retval_regularization + retval_loglikelihood;
}

void l2r_lr_mpi::grad(double *w, double *g) {
	NumericVector Rw(n);
	::memcpy(&Rw[0], w, sizeof(double) * n);
	boost::thread_group tgroup;
	tgroup.create_thread(boost::bind(&l2r_lr_mpi::grad_regularize, this, w, g));
	NumericVector g_loglikelihood(mpi_grad(wrap(Rw)));
	if (g_loglikelihood.size() != n) {
		throw std::logic_error("Inconsistent vector g_loglikelihood");
	}
	tgroup.join_all();
	for(int i = 0;i < n;i++) {
		g[i] += g_loglikelihood[i];
	}
}

void l2r_lr_mpi::Hv(double *s, double *Hs) {
	NumericVector Rs(n);
	::memcpy(&Rs[0], s, sizeof(double) * n);
	boost::thread_group tgroup;
	tgroup.create_thread(boost::bind(&l2r_lr_mpi::Hs_regularize, this, s, Hs));
	NumericVector Hs_loglikelihood(mpi_Hs(wrap(Rs)));
	if (Hs_loglikelihood.size() != n) {
		throw std::logic_error("Inconsistent vector Hs_loglikelihood");
	}
	tgroup.join_all();
	for(int i = 0;i < n;i++) {
		Hs[i] += Hs_loglikelihood[i];
	}
}

void l2r_lr_mpi::regularize(double *w, double* pretval) {
	double& retval(*pretval);
	retval = 0;
	for(int i = 0;i < n;i++) {
		retval += w[i]*w[i];
	}
	if (!is_regularize_bias) {
		retval -= w[n-1] * w[n-1];
	}
	retval /= 2.0;
}

void l2r_lr_mpi::grad_regularize(double *w, double *g) {
	for(int i = 0;i < n;i++) {
		g[i] = w[i];
	}
	if (!is_regularize_bias) {
		g[n-1] = 0;
	}
}

void l2r_lr_mpi::Hs_regularize(double *s, double* Hs) {
	for(int i = 0;i < n;i++) {
		Hs[i] = s[i];
	}
	if (!is_regularize_bias) {
		Hs[n-1] = 0;
	}
}

static void print_string_r(const char *s)
{
	Rprintf("%s", s);
}

RcppExport SEXP get_lr_mpi_host_funobj(SEXP Rmpi_fun, SEXP Rmpi_grad, SEXP Rmpi_Hs, SEXP Rlevels, SEXP Ris_regularize_bias) {
	BEGIN_RCPP
	XPtr<l2r_lr_mpi> retval(new l2r_lr_mpi(Rmpi_fun, Rmpi_grad, Rmpi_Hs, Rlevels, Ris_regularize_bias));
	return retval;
	END_RCPP
}

RcppExport SEXP lr_mpi_get_n(SEXP Rfun_obj) {
  BEGIN_RCPP
  return wrap(XPtr<l2r_lr_mpi>(Rfun_obj)->get_nr_variable());
  END_RCPP
}

RcppExport SEXP lr_mpi(SEXP Rfun_obj, SEXP Rtol) {
	BEGIN_RCPP
	XPtr<l2r_lr_mpi> fun_obj(Rfun_obj);
	TRON tron_obj(fun_obj.operator->(), as<double>(Rtol));
	tron_obj.set_print_string(&print_string_r);
	NumericVector Rw(fun_obj->get_nr_variable());
	Rw.fill(0);
	tron_obj.tron(&Rw[0]);
	return Rw;
	END_RCPP
}

RcppExport SEXP lr_mpi_init(SEXP Rfun_obj, SEXP Rw, SEXP Rtol) {
  BEGIN_RCPP
	XPtr<l2r_lr_mpi> fun_obj(Rfun_obj);
	NumericVector w0(Rw);
  NumericVector w(w0.size());
  ::memcpy(&w[0], &w0[0], w0.size() * sizeof(double));
  if (w.size() != fun_obj->get_nr_variable()) {
    throw std::invalid_argument("Invalid Length of Rw");
  }
	TRON tron_obj(fun_obj.operator->(), as<double>(Rtol));
	tron_obj.set_print_string(&print_string_r);
	tron_obj.tron(&w[0]);
	return w;
	END_RCPP
}
