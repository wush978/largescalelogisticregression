/*
 * lr.cpp
 *
 *  Created on: Jun 21, 2013
 *      Author: wush
 */


#include "lr.hpp"

//#define BAYLRL_DEBUG

using namespace Rcpp;


lr::lr(SEXP Ry, SEXP Rdata, SEXP Rlevel, SEXP RC)
: y(Ry), categorical_levels(Rlevel), C(RC) {
	BEGIN_RCPP
#ifdef BAYLRL_DEBUG
	Rprintf("Start lr::lr\n");
#endif
	DataFrame data(Rdata);
	if (y.size() != data.nrows())
		throw std::invalid_argument("The length of y and row of data are inconsistent");
	l = y.size();
	Function names("names");
	StrVec
		data_name(as<StrVec>(names(Rdata))),
		level_name(as<StrVec>(names(Rlevel)));
#ifdef BAYLRL_DEBUG
	Rprintf("Get names\n");
#endif
	int categorical_length_index = 0;
	for(StrVec::iterator i = data_name.begin();i != data_name.end();i++) {
		StrVec::iterator j = std::search(level_name.begin(), level_name.end(), i, i+1);
		if (level_name.end() == j) {
			// A numeric variable
			numerical_variable.push_back(NumericVector(wrap(data[*i])));
			continue;
		}
		// A categorical variable
		categorical_variable.push_back(IntegerVector(wrap(data[*i])));
		if (categorical_length_index++ != j - level_name.begin()) throw std::logic_error("Inconsistent categorical data ane names(Rlevels)");
	}
#ifdef BAYLRL_DEBUG
	Rprintf("Get variables\n");
#endif
	n = numerical_variable.size() + 1;
	for(int i = 0;i < categorical_levels.size();i++) {
		n += categorical_levels[i] - 1;
	}
#ifdef BAYLRL_DEBUG
	Rprintf("Get n\n");
#endif
	buffer.reserve(l);
	buffer.resize(l);
	D.reserve(l);
	D.resize(l);
  if (C.size() != y.size()) {
    throw std::invalid_argument("Size of C should be length(y)");
  }
  VOID_END_RCPP
}

lr::~lr() { }

double lr::fun_internal(SEXP Rw) {
  double *w = init_and_check(Rw);
  double retval = 0;
  for(int i = 0;i < l;i++) {
    retval += C[i] * ::log(1 + ::exp( - ywTx(i, w)));
  }
  return retval;
}

SEXP lr::fun(SEXP Rw) {
	BEGIN_RCPP
	return wrap<double>(fun_internal(Rw));
	END_RCPP
}

void lr::grad_internal(SEXP Rw, double* g) {
	double *w = init_and_check(Rw);
  for(int i = 0;i < l;i++) {
		D[i] = sigma(ywTx(i, w)); // for Hessian... Bad smell
		double scalar = C[i] * (D[i] - 1) * y_val(i);
		D[i] *= C[i] * (1 - D[i]);
		int offset = 0;
		for(int j = 0;j < numerical_variable.size();j++) {
			g[j] += scalar * numerical_variable[j][i];
		}
		offset += numerical_variable.size();
		for(int j = 0;j < categorical_variable.size();j++) {
			if (categorical_variable[j][i] > 1 & categorical_variable[j][i] < categorical_levels[j] + 1) {
				g[offset + categorical_variable[j][i] - 2] += scalar;
			}
			offset += categorical_levels[j] - 1;
		}
		g[n-1] += scalar;
	}
}

SEXP lr::grad(SEXP Rw) {
	BEGIN_RCPP
	NumericVector retval(n, 0.0);
  grad_internal(Rw, &retval[0]);
	return retval;
	END_RCPP
}

void lr::Hs_internal(SEXP Rs, double *retval) {
	double *s = init_and_check(Rs);
  int offset = 0;
	for(int i = 0;i < l;i++) {
		buffer[i] = wTx(i, s);
		buffer[i] *= D[i];
	}
  for(int i = 0;i < numerical_variable.size();i++) {
		for(int j = 0;j < l;j++) {
			retval[i] += buffer[j] * numerical_variable[i][j];
		}
	}
	offset += numerical_variable.size();
	for(int i = 0;i < categorical_variable.size();i++) {
		for(int j = 0;j < l;j++) {
			if (categorical_variable[i][j] > 1 & categorical_variable[i][j] < categorical_levels[i] + 1) {
				retval[offset + categorical_variable[i][j] - 2] += buffer[j];
			}
		}
		offset += categorical_levels[i] - 1;
	}
	for(int i = 0;i < l;i++) {
		retval[n - 1] += buffer[i];
	}
}

SEXP lr::Hs(SEXP Rs) {
	BEGIN_RCPP
//	double yz;
	NumericVector retval(n, 0.0);
  Hs_internal(Rs, &retval[0]);
	return retval;
	END_RCPP
}

double lr::wTx(int i, double* w) {
	double z = 0;
	int offset = 0;
	for(int j = 0;j < numerical_variable.size();j++) {
		z += w[j] * numerical_variable[j][i];
	}
	offset += numerical_variable.size();
	for(int j = 0;j < categorical_variable.size();j++) {
		if (categorical_variable[j][i] > 1 & categorical_variable[j][i] < categorical_levels[j] + 1) {
			z += w[offset + categorical_variable[j][i] - 2];
		}
		offset += categorical_levels[j] - 1;
	}
	z += w[n - 1];
	return z;
}

double* lr::init_and_check(SEXP Rw) {
	if (Rf_length(Rw) != n) {
		throw std::invalid_argument("Inconsistent Model Spec!");
	}
	return REAL(Rw);
}

RCPP_MODULE(lr) {
	class_<lr>("lr")
	.constructor<SEXP, SEXP, SEXP, SEXP>()
	.method("fun", &lr::fun)
	.method("grad", &lr::grad)
	.method("Hs", &lr::Hs)
	;
}
