#include <Rcpp.h>

using namespace Rcpp;

RcppExport SEXP check_na(SEXP Rdata) {
	BEGIN_RCPP
	
	DataFrame data(Rdata);
	int n = data.nrows();
	int m = data.size();
	for(int i = 0;i < m;i++) {
		try {
			int type = TYPEOF(wrap(data[i]));
			switch (type) {
				case INTSXP: {
					IntegerVector temp(wrap(data[i]));
					for (int j = 0;j < n;j++) {
						if (temp[j] == NA_INTEGER) return wrap(false);
					}
					continue;
				}
				case REALSXP: {
					NumericVector temp(wrap(data[i]));
					for (int j = 0;j < n;j++) {
						if (temp[j] == NA_REAL) return wrap(false);
					}
					continue;
				}
				default: {
					Rprintf("Error column: %d\n", i + 1);
					throw std::invalid_argument("The column of data should be one of \"numeric\" or \"factor\"");
				}
			}
		}
		catch (std::exception &e) {
			Rprintf("Error occurred at column: %d\n", i);
			throw e;
		}
	}
	return wrap(true);
	END_RCPP
}