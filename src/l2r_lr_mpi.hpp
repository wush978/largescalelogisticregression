/*
 * l2r_lr_mpi.hpp
 *
 *  Created on: Jun 22, 2013
 *      Author: wush
 */

#ifndef L2R_LR_MPI_HPP_
#define L2R_LR_MPI_HPP_

#include "tron.h"
#include <Rcpp.h>

typedef std::vector<std::string> StrVec;
typedef std::vector<int> IntVec;

class l2r_lr_mpi: public function
{
private:
	Rcpp::Function mpi_fun, mpi_grad, mpi_Hs;
	int n;
	bool is_regularize_bias;

public:
	l2r_lr_mpi(SEXP Rmpi_fun, SEXP Rmpi_grad, SEXP Rmpi_Hs, SEXP Rlevels, SEXP Ris_regularize_bias);

	~l2r_lr_mpi();

	double fun(double *w);
	void grad(double *w, double *g);
	void Hv(double *s, double *Hs);

	int get_nr_variable(void) {
		return n;
	}


private:
	void regularize(double *w, double* retval);
	void grad_regularize(double* w, double* g);
	void Hs_regularize(double* s, double* Hs);
};



#endif /* L2R_LR_MPI_HPP_ */
