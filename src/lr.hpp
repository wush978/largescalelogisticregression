/*
 * lr.hpp
 *
 *  Created on: Jun 21, 2013
 *      Author: wush
 */

#ifndef LR_HPP_
#define LR_HPP_


#include <vector>
#include <Rcpp.h>

class lr {

private:
	Rcpp::LogicalVector y;
	std::vector<Rcpp::NumericVector> numerical_variable;
	std::vector<Rcpp::IntegerVector> categorical_variable;
	Rcpp::IntegerVector categorical_levels;
	int n; // number of variables
	int l; // number of local instances
	std::vector<double> buffer, D;
	Rcpp::NumericVector C;

public:
	lr(SEXP Ry, SEXP Rdata, SEXP Rlevel, SEXP RC);
	~lr();
  double fun_internal(SEXP Rw);
	inline SEXP fun(SEXP Rw);
  void grad_internal(SEXP Rw, double* g);
	inline SEXP grad(SEXP Rw);
  void Hs_internal(SEXP Rs, double *s);
	inline SEXP Hs(SEXP Rs);
  inline int get_n() const {return n;}

private:
	double wTx(int i, double* w);

	inline double ywTx(int i, double *w) {
		return (y[i] ? wTx(i, w) : -wTx(i, w));
	}

	static inline double sigma(double x) {
		return 1/(1 + exp(-x));
	}

	inline double y_val(int i) {
		return (y[i] ? 1.0 : -1.0);
	}

	double* init_and_check(SEXP Rw);
};

typedef std::vector<std::string> StrVec;
typedef std::vector<int> IntVec;

#endif /* LR_HPP_ */
