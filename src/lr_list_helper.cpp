#include <Rcpp.h>
#include "lr.hpp"

using namespace Rcpp;

SEXP extract_obj_ptr(SEXP Rsrc) {
  RObject src(Rsrc);
  Environment xData(wrap(src.slot(".xData")));
  return wrap(xData[".pointer"]);
}

//'@export
//[[Rcpp::export]]
SEXP list_fun(List worker_list, SEXP w) {
  BEGIN_RCPP
  double retval = 0;
  for(int i = 0;i < worker_list.size();i++) {
    retval += XPtr<lr>(extract_obj_ptr(wrap(worker_list[i])))->fun_internal(w);
  }
  return wrap(retval);
  END_RCPP
}

//'@export
//[[Rcpp::export]]
SEXP list_grad(List worker_list, SEXP w) {
  BEGIN_RCPP
  if (worker_list.size() == 0) {
    throw std::invalid_argument("Empty List!");
  }
  int n = XPtr<lr>(extract_obj_ptr(wrap(worker_list[0])))->get_n();
  NumericVector retval(n, 0.0);
  for(int i = 0;i < worker_list.size();i++) {
    XPtr<lr>(extract_obj_ptr(wrap(worker_list[i])))->grad_internal(w, &retval[0]);
  }
  return retval;
  END_RCPP
}

//'@export
//[[Rcpp::export]]
SEXP list_Hs(List worker_list, SEXP w) {
  BEGIN_RCPP
  if (worker_list.size() == 0) {
    throw std::invalid_argument("Empty List!");
  }
  int n = XPtr<lr>(extract_obj_ptr(wrap(worker_list[0])))->get_n();
  NumericVector retval(n, 0.0);
  for(int i = 0;i < worker_list.size();i++) {
    XPtr<lr>(extract_obj_ptr(wrap(worker_list[i])))->Hs_internal(w, &retval[0]);
  }
  return retval;
  END_RCPP
}