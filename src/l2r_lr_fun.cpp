/*
 * bayesian_lr.cpp
 *
 *  Created on: May 30, 2013
 *      Author: wush
 */


#include "tron.h"
#include <Rcpp.h>
#include <memory>

//#define BAYLRL_DEBUG

using namespace Rcpp;

typedef std::vector<std::string> StrVec;
typedef std::vector<int> IntVec;

class l2r_lr_fun: public function
{
private:
//	void Xv(double *v, double *Xv);
//	void XTv(double *v, double *XTv);
	LogicalVector y;
	std::vector<NumericVector> numeric_variable;
	std::vector<IntegerVector> catagorical_variable;
	IntegerVector catagorical_levels;
	int n; // number of variables
	int l; // number of instances
	std::vector<double> buffer, D;
  bool is_regularize_bias;
  NumericVector C;
#ifdef BAYLRL_DEBUG
	void dump_vec(const char* name, double* w, int w_len);
#endif


public:
	l2r_lr_fun(SEXP Ry, SEXP Rdata, SEXP Rlevel, SEXP Ris_regularize_bias, SEXP RC)
	: y(Ry), catagorical_levels(Rlevel), is_regularize_bias(as<bool>(Ris_regularize_bias)), C(RC)
	{
		DataFrame data(Rdata);
		if (y.size() != data.nrows()) throw std::invalid_argument("The length of y and row of data are inconsistent");
#ifdef BAYLRL_DEBUG
		Rprintf("y:\n");
		for(int i = 0;i < y.size();i++) {
			if (y[i]) 
				Rprintf("T ");
			else
				Rprintf("F ");
		}
		Rprintf("\n");
#endif
		l = y.size();
		List level(Rlevel);
		Function names("names");
		StrVec
			data_name(as<StrVec>(names(Rdata))),
			level_name(as<StrVec>(names(Rlevel)));
		int catagorical_length_index = 0;
		for(StrVec::iterator i = data_name.begin();i != data_name.end();i++) {
			StrVec::iterator j = std::search(level_name.begin(), level_name.end(), i, i+1);
			if (level_name.end() == j) {
				// A numeric variable
				numeric_variable.push_back(NumericVector(wrap(data[*i])));
				continue;
			}
			// A catagorical variable
			catagorical_variable.push_back(IntegerVector(wrap(data[*i])));
//			catagorical_levels.push_back(::Rf_length(level[*i]));
			if (catagorical_length_index++ != j - level_name.begin()) throw std::logic_error("Inconsistent catagorical data ane names(Rlevels)");
		}
		n = numeric_variable.size() + 1;
		for(int i = 0;i < catagorical_levels.size();i++) {
			n += catagorical_levels[i] - 1;
		}
		buffer.reserve(l);
		buffer.resize(l);
		D.reserve(l);
		D.resize(l);
    if (C.size() != 1 & C.size() != y.size()) {
      throw std::invalid_argument("Size of C should be 1 or length(y)");
    }
	}

	~l2r_lr_fun() { }

	double fun(double *w) {
		int w_size = get_nr_variable();
#ifdef BAYLRL_DEBUG
		dump_vec("w", w, w_size);
#endif
		double f = 0;
		for(int i = 0;i < w_size;i++) {
			f += w[i] * w[i];
		}
    if (!is_regularize_bias) {
      f -= w[w_size - 1] * w[w_size - 1];
    }
		f /= 2.0;
    if (C.size() == 1) {
  		for(int i = 0;i < l;i++) {
  			f += C[0] * log(1 + exp(- ywTx(i, w)));
  		}
    }
    else {
    	for(int i = 0;i < l;i++) {
  			f += C[i] * log(1 + exp(- ywTx(i, w)));
  		}
    }
#ifdef BAYLRL_DEBUG
		Rprintf("%f\n", f);
#endif
		return f;
	}
	void grad(double *w, double *g) {
		int w_size = get_nr_variable();
		for(int i = 0;i < w_size;i++) {
			g[i] = w[i];
		}
    if (!is_regularize_bias) {
      g[w_size - 1] = 0;
    }
		for(int i = 0;i < l;i++) {
			D[i] = sigma(ywTx(i, w)); // for Hessian... Bad smell
			double scalar = (C.size() == 1 ? C[0] : C[i]) * (D[i] - 1) * y_val(i);
			D[i] *= (C.size() == 1 ? C[0] : C[i]) * (1 - D[i]);
			int offset = 0;
			for(int j = 0;j < numeric_variable.size();j++) {
				g[j] += scalar * numeric_variable[j][i];
			}
			offset += numeric_variable.size();
			for(int j = 0;j < catagorical_variable.size();j++) {
				if (catagorical_variable[j][i] > 1 & catagorical_variable[j][i] < catagorical_levels[j] + 1) {
					g[offset + catagorical_variable[j][i] - 2] += scalar;
				}
				offset += catagorical_levels[j] - 1;
			}
			g[w_size-1] += scalar;
		}
#ifdef BAYLRL_DEBUG
		dump_vec("g", g, w_size);
#endif
	}
	void Hv(double *s, double *Hs) {
		int w_size = get_nr_variable();
#ifdef BAYLRL_DEBUG
		dump_vec("s", s, w_size);
#endif
		double yz;
		for(int i = 0;i < l;i++) {
			buffer[i] = wTx(i, s);
			buffer[i] *= D[i];
		}
#ifdef BAYLRL_DEBUG
//		dump_vec("DXs", &buffer[0], l);
#endif
		int offset = 0;
		for(int i = 0;i < w_size;i++) {
			Hs[i] = s[i];
		}
    if (!is_regularize_bias) {
      Hs[w_size - 1] = 0;
    }
		for(int i = 0;i < numeric_variable.size();i++) {
			for(int j = 0;j < l;j++) {
				Hs[i] += buffer[j] * numeric_variable[i][j];
			}
		}
		offset += numeric_variable.size();
		for(int i = 0;i < catagorical_variable.size();i++) {
			for(int j = 0;j < l;j++) {
				if (catagorical_variable[i][j] > 1 & catagorical_variable[i][j] < catagorical_levels[i] + 1) {
					Hs[offset + catagorical_variable[i][j] - 2] += buffer[j];
				}
			}
			offset += catagorical_levels[i] - 1;			
		}
		for(int i = 0;i < l;i++) {
			Hs[w_size - 1] += buffer[i];
		}
#ifdef BAYLRL_DEBUG
		dump_vec("Hs", Hs, w_size);
#endif
	}

	int get_nr_variable(void) {
		return n;
	}

private:
	double wTx(int i, double* w) {
		double z = 0;
		int offset = 0;
		for(int j = 0;j < numeric_variable.size();j++) {
			z += w[j] * numeric_variable[j][i];
		}
		offset += numeric_variable.size();
		for(int j = 0;j < catagorical_variable.size();j++) {
			if (catagorical_variable[j][i] > 1 & catagorical_variable[j][i] < catagorical_levels[j] + 1) {
				z += w[offset + catagorical_variable[j][i] - 2];
			}
			offset += catagorical_levels[j] - 1;
		}
		z += w[get_nr_variable() - 1];
		return z;
	}
	
	inline double ywTx(int i, double* w) {
		return (y[i] ? wTx(i, w) : -wTx(i, w));
	}
	
	inline double sigma(double x) {
		return 1/(1 + exp(-x));
	}
	
	inline double y_val(int i) {
		return (y[i] ? 1.0 : -1.0);
	}
};

#ifdef BAYLRL_DEBUG
void l2r_lr_fun::dump_vec(const char* name, double* w, int w_len) {
	Rprintf("%s: \n", name);
	for(int i = 0;i < w_len;i++) {
		Rprintf("%f ", w[i]);
	}
	Rprintf("\n");
}
#endif
static void print_string_r(const char *s)
{
	Rprintf("%s", s);
}

RcppExport SEXP lr(SEXP Ry, SEXP Rdata, SEXP Rlevel, SEXP Rtol, SEXP Ris_regularize_bias, SEXP RC) {
	BEGIN_RCPP

	std::auto_ptr< ::function > fun_obj(new l2r_lr_fun(Ry, Rdata, Rlevel, Ris_regularize_bias, RC));
	TRON tron_obj(fun_obj.operator->(), as<double>(Rtol));
	tron_obj.set_print_string(&print_string_r);
	NumericVector Rw(fun_obj->get_nr_variable());
	Rw.fill(0);
	tron_obj.tron(&Rw[0]);
	return Rw;
	
	END_RCPP
}

RcppExport SEXP lr_predict(SEXP Rw, SEXP Rdata, SEXP Rn_numerical, SEXP Rname, SEXP Rlevel_name, SEXP Rlevels) {
	BEGIN_RCPP
	NumericVector w(Rw);
	DataFrame data(Rdata);
	List levels(Rlevels);
	int n_numerical(as<int>(Rn_numerical));
	CharacterVector level_name(Rlevel_name), name(Rname);
	
	std::vector<int> levels_len(levels.size());
	// check data
	int n = data.nrows();
	std::vector<NumericVector> num_vec;
	std::vector<IntegerVector> cata_vec;
	num_vec.reserve(n_numerical);
	cata_vec.reserve(levels.size());
	for(int j = 0;j < n_numerical;j++) {
		const char* var_name = CHAR(wrap(name[j]));
		num_vec.push_back(NumericVector(wrap(data[var_name])));
		if (num_vec[j].size() != n) {
			std::string msg("Missing column: \"");
			msg.append(var_name).append("\"");
			throw std::invalid_argument(msg.c_str());
		}
	}
	for(int j = 0;j < levels.size();j++) {
		const char* var_name = CHAR(wrap(level_name[j]));
		cata_vec.push_back(IntegerVector(wrap(data[var_name])));
		if (cata_vec[j].size() != n) {
			std::string msg("Missing column: \"");
			msg.append(var_name).append("\"");
			throw std::invalid_argument(msg.c_str());
		}
		levels_len[j] = Rf_length(wrap(levels[j]));
	}
	
	//start prediction
	NumericVector retval(n);
	double value;
	for(int i = 0;i < n;i++) {
		// constant
		value = w[w.size() - 1];
		int offset = 0;
		//numerical
		for(int j = 0;j < num_vec.size();j++) {
			value += w[j] * num_vec[j][i];
//			Rprintf("numerical: %4.4f %4.4f ==> %4.4f\n", w[j], num_vec[j][i], value);
		}
		offset += num_vec.size();
		//catagorical
//		Rprintf("===%d===\n", i+1);
		for(int j = 0;j < cata_vec.size();j++) {
//			Rprintf("catagorical(%s, %d):", CHAR(wrap(level_name[j])), cata_vec[j][i]);
			if (cata_vec[j][i] > 1 & cata_vec[j][i] < levels_len[j] + 1) {
				value += w[offset + cata_vec[j][i] - 2];
//				Rprintf(" %d ==> %4.4f ==> %4.4f\n", cata_vec[j][i], w[offset + cata_vec[j][i] - 2], value);
			}
//			else {
//				Rprintf("\n");
//			}
			offset += levels_len[j] - 1;
		}
		retval[i] = value;
//		Rprintf("--final-- value: %4.4f retval[i]: %4.4f\n", value, retval[i]);
	}
//	Rprintf("\n");
	
	return retval;
	END_RCPP
}