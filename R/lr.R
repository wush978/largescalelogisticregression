#'L2 Penalized Logistic Regression
#'
#'@param y logical vector, the response variable
#'@param data data.frame with numeric or factor vectors, the explanatory variables
#'@param tol numeric vector, the tolerance of termination criterion for optimization.
#'@param is_regularize_bias logical value, if intercept term is regularized or not
#'@param C numeric vector. If C is single value, then it is the pentalty of logliklihood. 
#'If C is numeric vector whose length is \code{length(y)}, then C is the data weight.
#'@export
lr.logical <- function(y, data, tol = 0.002650, is_regularize_bias = TRUE, C = 1.0) {
	stopifnot(inherits(y, "logical"))
	stopifnot(inherits(data, "data.frame"))
	stopifnot(class(tol) == "numeric")
	stopifnot(length(tol) == 1)
	stopifnot(sapply(data, function(a) {
		inherits(a, "numeric") | inherits(a, "factor")
	}))
	stopifnot(inherits(tol, "numeric"))
	stopifnot(.Call("check_na", data))
	catagorical_feature_index <- which(sapply(data, function(a) {
		inherits(a, "factor")
	}))
	character_feature_levels_length <- sapply(data, function(a) length(levels(a)))
	character_feature_levels_length <- character_feature_levels_length[catagorical_feature_index]
	retval <- new("lr_model")
	retval@w <- .Call("lr", y, data, character_feature_levels_length, tol[1], is_regularize_bias, C)
	temp <- generate_name(data)
	retval@name <- temp$name
	retval@levels <- temp$levels
	stopifnot(length(retval@w) == length(retval@name))
	retval
}

