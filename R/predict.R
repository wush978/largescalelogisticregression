#'@export
predict.lr_model <- function(m, data) {
# 	browser()
	for(name in names(m@levels)) {
		if (class(data[[name]]) == "factor") {
			stopifnot(levels(data[[name]]) == m@levels[[name]])
		}
	}
  if (length(m@levels) > 0) {
	  n.numerical <- length(m@w) - (sum(sapply(m@levels, length) - 1) + 1)
  } else {
    n.numerical <- length(m@w)- 1
  }
#   browser()
  if (length(m@levels) == 0) {
    return(.Call("lr_predict", m@w, data, n.numerical, m@name, "", m@levels))
  } else {
	  return(.Call("lr_predict", m@w, data, n.numerical, m@name, names(m@levels), m@levels))
  }
}