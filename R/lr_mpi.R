worker_env <- new.env()

#'@export
mpi_init.worker <- function(y.name, data.name, C.name) {
	catagorical_feature_index <- which(sapply(eval(data.name), function(a) {
		inherits(a, "factor")
	}))
	character_feature_levels_length <- sapply(eval(data.name), function(a) length(levels(a)))
	character_feature_levels_length <- character_feature_levels_length[catagorical_feature_index]
	worker_env$funobj <- new(lr, eval(y.name), eval(data.name), character_feature_levels_length, eval(C.name))
}

#'@export
mpi_fun.worker <- function(w) {
	worker_env$funobj$fun(w)
}

#'@export
mpi_grad.worker <- function(w) {
	worker_env$funobj$grad(w)
}

#'@export
mpi_Hs.worker <- function(s) {
	worker_env$funobj$Hs(s)
}


#'@export
lr.cluster <- function(cl, y.name, data.name, C.name, tol = 0.002650, is_regularize_bias = TRUE) {
	stopifnot(is.name(y.name))
	stopifnot(is.name(data.name))
	stopifnot(is.name(C.name))
	stopifnot(unlist(clusterEvalQ(cl, {
		require(LargeScaleLR)
	})))
	clusterExport(cl, c("y.name", "data.name", "C.name"), envir=environment())
	stopifnot(unlist(clusterEvalQ(cl, {
		exists(as.character(y.name))
	})))
	stopifnot(unlist(clusterEvalQ(cl, {
		is.logical(eval(y.name))
	})))
	stopifnot(unlist(clusterEvalQ(cl, {
		exists(as.character(data.name))
	})))
	stopifnot(unlist(clusterEvalQ(cl, {
		inherits(eval(data.name), "data.frame")
	})))
	stopifnot({# check whether data name/type is consistent
		data.type <- clusterEvalQ(cl, {
			sapply(eval(data.name), class)
		})
		if (length(data.type) == 1) {
			TRUE
		} else {
			all(sapply(2:length(data.type), function(i) all.equal(data.type[[1]], data.type[[i]])))
		}
	})
	stopifnot({ # check if levels are consistent
		data.level <- clusterEvalQ(cl, {
			 sapply(eval(data.name), levels)
		})
		if (length(data.level) == 1) {
			TRUE
		} else {
			all(sapply(2:length(data.level), function(i) all.equal(data.level[[1]], data.level[[i]])))
		}
	})
	stopifnot(unlist(clusterEvalQ(cl, {
		exists(as.character(C.name))
	})))
	stopifnot(unlist(clusterEvalQ(cl, {
		is.numeric(eval(C.name))
	})))
	stopifnot(unlist(clusterEvalQ(cl, {
		nrow(eval(data.name)) == length(eval(C.name))
	})))
	clusterEvalQ(cl, {
		mpi_init.worker(y.name, data.name, C.name)
	})
	mpi_fun <- function(w) {
		loglikelihood.map <- unlist(clusterCall(cl=cl, fun=mpi_fun.worker, w))
# 		print("loglikelihood.map")
# 		print(loglikelihood.map)
		sum(loglikelihood.map)
	}
	mpi_grad <- function(w) {
		g <- clusterCall(cl=cl, fun=mpi_grad.worker, w)
# 		print("g")
# 		print(g)
		stopifnot(is.list(g))
		retval <- Reduce("+", g)
# 		print(retval)
		retval
	}
	mpi_Hs <- function(s) {
		Hs <- clusterCall(cl=cl, fun=mpi_Hs.worker, s)
# 		print("Hs")
# 		print(Hs)
		retval <- Reduce("+", Hs)
# 		print(retval)
		retval
	}
	retval <- new("lr_model")
	retval@levels <- data.level[[1]][!sapply(data.level[[1]], is.null)]
	lr_mpi_funobj <- .Call("get_lr_mpi_host_funobj", mpi_fun, mpi_grad, mpi_Hs, data.level[[1]], is_regularize_bias)
	retval@w <- .Call("lr_mpi", lr_mpi_funobj, tol)
	retval@name <- name_level(data.type[[1]], data.level[[1]])
	retval
}

